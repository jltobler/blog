---
title: "About"
ShowReadingTime: false
---

Hello world, I'm Justin and welcome to my blog. This is a space where I intend
to write about some of my current projects and interests. A little about myself,
I'm a backend software engineer at [GitLab](https://gitlab.com/) maintaining and
contributing to the [Gitaly](https://gitlab.com/gitlab-org/gitaly) project.
