---
title: "Welcome!"
date: 2023-09-16T17:52:20-05:00
---

Hello and welcome to by blog where I intend to write about my current projects 
and interests. I've never written blog posts before, so I guess this counts as 
my first post. I don't think this post is going to have much content, but I
needed something to publish with as a test. Maybe my next post will discuss how
I'm deploying this blog.
